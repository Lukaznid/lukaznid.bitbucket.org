
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

//AIzaSyD4RWUP-4vm1xtZPFrhOf7umffl5cUmPxA <- ključ za google api



/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

$(document).ready(function() {
    
    /* Ustvarjanje uporabnikov */
    
    document.getElementById("gen_gumb").onclick = function() {generirajUporabnike()};
    document.getElementById("vnosGumb").onclick=function() {
        izbiraERHID();
    };
    var genUporabniki = [{},{},{}]; //uporabnik1,uporabnik2,uporabnik3
    var zeObstaja=0;
    
    
    generirajUporabnike=function(){
        $("#generiranje").html("");
        document.getElementById('generiranjeCela').insertAdjacentHTML('beforebegin', '<div id="imena"></div>');
        for(var i=1; i<=3; i++){
          dodajGumbe(i,kreirajEHRzaBolnika); //doda gumbe in druge funkcionalnosti
          
        }
    }
     
    var EHRId="";
      
    function dodajGumbe(stPacienta, callback) {
      var pot="";
      var ime="";
      switch (stPacienta){
        case 1:
            pot="knjiznice/resources/Marija_Default.jpg";
            genUporabniki[0]={
                EHRId: "",
                ime: "Marija",
                priimek: "D'fault",
                datumRojstva: "1938-10-30T14:58",
                visina: "173",
                teza: "65",
                temperatura: "36",
                sisKrvTlak: "20",
                diasKrvTlak: "30",
            }
            //EHRId=kreirajEHRzaBolnika(stPacienta);
            break;
        case 2:
            pot="knjiznice/resources/Darja_Skrivalec.jpg";
            genUporabniki[1]={
                EHRId: "",
                ime: "Darja",
                priimek: "Skrivalec",
                datumRojstva: "1938-10-30T14:58",
                visina: "163",
                teza: "55",
                temperatura: "55",
                sisKrvTlak: "20",
                diasKrvTlak: "30",
            }
            break;
        case 3:   
            pot="knjiznice/resources/Damjan_Pjevar.jpg";
            genUporabniki[2]={
                EHRId: "",
                ime: "Damjan",
                priimek: "Pjevar",
                datumRojstva: "1938-10-30T14:58",
                visina: "120",
                teza: "40",
                temperatura: "35",
                sisKrvTlak: "28",
                diasKrvTlak: "12",
            }
            break;
      }
      callback(stPacienta, pot);
    }
    
    function kreirajEHRzaBolnika(stPacienta, pot) {
    	sessionId = getSessionId();
        var ime=genUporabniki[stPacienta-1].ime;
        var priimek=genUporabniki[stPacienta-1].priimek;
        var datumRojstva=genUporabniki[stPacienta-1].datumRojstva;
        
    	if (!ime || !priimek || !datumRojstva || 
    	  ime.trim().length == 0 || priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
    		alert("napaka pri vnosu podatkov");
    	} else {
    		$.ajaxSetup({
    		    headers: {"Ehr-Session": sessionId}
    		});
    		$.ajax({
    		    url: baseUrl + "/ehr",
    		    type: 'POST',
    		    success: function (data) {
    		        var ehrId = data.ehrId;
    		        var partyData = {
    		            firstNames: ime,
    		            lastNames: priimek,
    		            dateOfBirth: datumRojstva,
    		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
    		        };
    		        $.ajax({
    		            url: baseUrl + "/demographics/party",
    		            type: 'POST',
    		            contentType: 'application/json',
    		            data: JSON.stringify(partyData),
    		            success: function (party) {
    		                if (party.action == 'CREATE') {
    		                    genUporabniki[stPacienta-1].EHRId=ehrId; //se doda
    		                    ime=genUporabniki[stPacienta-1].ime+" "+genUporabniki[stPacienta-1].priimek;
    		                    
                                $("#generiranje").append("<img src="+pot+" class=pacient-slika id="+ehrId+">");
                                $("#imena").append('<p class="ime">'+ime+'</p>');
                                
                                document.getElementById(ehrId).addEventListener("click", function (){
                                    if(zeObstaja<1){
                                        //document.getElementById('generiranjeCela').insertAdjacentHTML('beforebegin', '<div id="rezultat"></div>');
                                        $("#glavna").append('<div id="rezultat"></div>');
                                        zeObstaja++;
                                    }else{
                                        $("#rezultat").html("");
                                    }
                                $("#rezultat").append('<p>Izbrali ste '+genUporabniki[stPacienta-1].ime+" "+genUporabniki[stPacienta-1].priimek+" "+'z ERHId-jem '+genUporabniki[stPacienta-1].EHRId+' </p>');
                                });
                                
                                dodajMeritveVitalnihZnakov(stPacienta-1,ehrId);
    		                }
    		            },
    		            error: function(err) {
    		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                        "label-danger fade-in'>Napaka '" +
                        JSON.parse(err.responseText).userMessage + "'!");
    		            }
    		        });
    		    }
    		});
    	}
    }
    
    function dodajMeritveVitalnihZnakov(stPacienta, ehrId) {
    	sessionId = getSessionId();
    	
    	var telesnaVisina = genUporabniki[stPacienta].visina;
    	var telesnaTeza = genUporabniki[stPacienta].visina;
    	var telesnaTemperatura = genUporabniki[stPacienta].temperatura;
    	var sistolicniKrvniTlak = genUporabniki[stPacienta].sisKrvTlak;
    	var diastolicniKrvniTlak = genUporabniki[stPacienta].diasKrvTlak;
    
    	if (!ehrId || ehrId.trim().length == 0) {
    		alert("Manjka EHRId pri dodjanju dodatnih podatkov");
    	} else {
    		$.ajaxSetup({
    		    headers: {"Ehr-Session": sessionId}
    		});
    		var podatki = {
    		    "ctx/language": "en",
    		    "ctx/territory": "SI",
    		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
    		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
    		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
    		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
    		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak
    		};
    		var parametriZahteve = {
    		    ehrId: ehrId,
    		    templateId: 'Vital Signs',
    		    format: 'FLAT',
    		    committer: 'josko'
    		};
    		$.ajax({
    		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
    		    type: 'POST',
    		    contentType: 'application/json',
    		    data: JSON.stringify(podatki),
    		    success: function (res) {
    		    },
    		    error: function(err) {
    		    	alert('napaka pri dodanjanju info o pacientih v podatkovno bazo!');
    		    }
    		});
    	}
    }

    
    /* Pošiljanje ERD-Id na api in prikaz vitalnih znakov pacienta */
    izbiraERHID=function(){
        $("#temp").html("");
        $("#osPodatki").html("");
        $("#temp").append('<p style="text-align: center; color: white;">Temperatura</p>');
        $("#temp").append('<canvas id="tempChart" width="740" height="300"></canvas>');
        $("#teza").html("");
        $("#temp").append('<p style="text-align: center; color: white;">Teza</p>');
        $("#teza").append('<canvas id="tezaChart" width="740" height="300"></canvas>')
        $("#oseba").html("");
        var izbranErh=document.getElementById('vnos').value;
        if(izbranErh==""){
            alert("Vnesite EHR-Id");
        }else{
            preberiEHRodBolnika(izbranErh,preberiMeritveVitalnihZnakov);
        }
    }
    
    function preberiEHRodBolnika(ehrId,callback) {
    	sessionId = getSessionId();
    	if (!ehrId || ehrId.trim().length == 0) {
    		alert("Prosim vnesite ErhId, ni mi jasno kako za vraga ste prišli mimo prvega pregleda ampk ok, saj ni važno.");
    	} else {
    		$.ajax({
    			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    			type: 'GET',
    			headers: {"Ehr-Session": sessionId},
    	    	success: function (data) {
    				var party = data.party;
    				datum=party.dateOfBirth;
                    $("#oseba").append('<p class="lead">'+party.firstNames+" "+party.lastNames+'</p>');
                    $("#osPodatki").append("<p>Datum rojstva: "+datum+"</p>");
                    callback(ehrId,"telesna temperatura");
                    callback(ehrId,"telesna teža");
                    callback(ehrId,"visina");
                    callback(ehrId,"krvniTlak");
                    callback(ehrId,"starost");
    			},
    			error: function(err) {
    				alert("Vnesli ste neveljaven EhrId!");
    			}
    		});
    	}
    }
    
    function preberiMeritveVitalnihZnakov(ehrId,tip) {
    	sessionId = getSessionId();
    
    	if (!ehrId || ehrId.trim().length == 0 || !tip || tip.trim().length == 0) {
    		$("#oseba").html("<span>Prišlo je do napake pri pridobitvi podatkov</span>");
    	} else {
    		$.ajax({
    			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	    	type: 'GET',
    	    	headers: {"Ehr-Session": sessionId},
    	    	success: function (data) {
    				var party = data.party;
                      
    				if (tip == "telesna temperatura") {
    				    
    					$.ajax({
      					    url: baseUrl + "/view/" + ehrId + "/" + "body_temperature",
    					    type: 'GET',
    					    headers: {"Ehr-Session": sessionId},
    					    success: function (res) {
    					    	if (res.length > 0) {
    						        var myres = JSON.stringify(res);
    						        var ctx = document.getElementById("tempChart").getContext("2d");
    						        var podatki=[res.length];
    						        var podatki2=[res.length];
    						        var barva=[res.length];
    						        var barva2=[res.length];
    						        
    						        for(var i=0; i<res.length; i++){
    						            podatki[i]=res[i].temperature;
    						            podatki2[i]=res[i].time;
    						            var bar='rgba('+Math.floor(Math.random()*255)+','+
    						                            Math.floor(Math.random()*255)+','+
    						                            Math.floor(Math.random()*255)+', ';
    						            barva[i]=bar+'0.2)';
    						            barva2[i]=bar+'1)';
    						        }
    						        
    						        var myChart = new Chart(ctx, {
                                        type: 'bar',
                                        data: {
                                            labels: podatki2,
                                            datasets: [{
                                                label: 'temperatura v '+res[0].unit,
                                                data: podatki,
                                                backgroundColor: barva,
                                                borderColor: barva2,
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero:true
                                                    }
                                                }]
                                            }
                                        }
                                    });
    					    	} else {
    					    		$("#temp").html("<p>Ni podatkov o telesni temperaturi.</p>");
    					    	}
    					    },
    					    error: function() {
    					    	$("#oseba").html("<span>Prišlo je do napake pri pridobitvi podatkov</span>");
    					    }
    					});
    					
    				} else if (tip == "telesna teža") {
    				    
    					$.ajax({
    					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    					    type: 'GET',
    					    headers: {"Ehr-Session": sessionId},
    					    success: function (res) {
    					    	if (res.length > 0) {
    						        var myres = JSON.stringify(res);
    						        var ctx = document.getElementById("tezaChart").getContext("2d");
    						        
    						        var podatki=[res.length];
    						        var podatki2=[res.length];
    						        var barva=[res.length];
    						        var barva2=[res.length];
    						        
    						        for(var i=0; i<res.length; i++){
    						            podatki[i]=res[i].weight;
    						            podatki2[i]=res[i].time;
    						            var bar='rgba('+Math.floor(Math.random()*255)+','+
    						                            Math.floor(Math.random()*255)+','+
    						                            Math.floor(Math.random()*255)+', ';
    						            barva[i]=bar+'0.2)';
    						            barva2[i]=bar+'1)';
    						        }
    						        
    						        var myChart = new Chart(ctx, {
                                        type: 'bar',
                                        data: {
                                            labels: podatki2,
                                            datasets: [{
                                                label: 'Teza v '+res[0].unit,
                                                data: podatki,
                                                backgroundColor: barva,
                                                borderColor: barva2,
                                                borderWidth: 1
                                            }]
                                        },
                                        options: {
                                            scales: {
                                                yAxes: [{
                                                    ticks: {
                                                        beginAtZero:true
                                                    }
                                                }]
                                            }
                                        }
                                    });
    					    	} else {
    					    		$("#teza").html("<p>Ni podatkov o telesni temperaturi.</p>");
    					    	}
    					    },
    					    error: function() {
    					    	$("#oseba").html("<span>Prišlo je do napake pri pridobitvi podatkov</span>");
    					    }
    					});
    				} else if (tip=="visina"){
    				    $.ajax({
      					    url: baseUrl + "/view/" + ehrId + "/" + "height",
    					    type: 'GET',
    					    headers: {"Ehr-Session": sessionId},
    					    success: function (res) {
    					    	if (res.length > 0) {
    					    	    var myres = JSON.stringify(res);
    						        $("#osPodatki").append('<p class="podatek">Višina: '+res[res.length-1].height+'</p>');
    						        
    					    	} else {
    					    		$("#temp").html("<p>Ni podatkov o telesni temperaturi.</p>");
    					    	}
    					    },
    					    error: function() {
    					    	alert("noup");
    					    }
    					});
    				} else if(tip=="krvniTlak"){
    				    $.ajax({
                            url: baseUrl + "/view/" + ehrId + "/blood_pressure",
                            type: 'GET',
                            headers: {"Ehr-Session": sessionId},
                            success: function (res) {
                                if(res.length > 0){
                                    var myres = JSON.stringify(res);
                                    var sistolicni=res[res.length-1].systolic;
                                    var diastolicni=res[res.length-1].diastolic;
                                    $("#osPodatki").append("<p>Diastolicni krvni tlak: "+sistolicni+"</p>");
                                    $("#osPodatki").append("<p>Sistolicni krvni tlak: "+diastolicni+"</p>");
                                }else{
                                    
                                }
                            }
                        });
    				}
    				else if(tip=="starost"){
    				    $.ajax({
                            url: baseUrl + "/view/" + ehrId + "/age",
                            type: 'GET',
                            headers: {"Ehr-Session": sessionId},
                            success: function (res) {
                                if(res.length > 0){
                                    var myres = JSON.stringify(res);
                                    alert(myres);
                                    /*for (var i in res) {
                                        $("#result").append(res[i].time + ': ' + res[i].systolic + '/' + res[i].diastolic + res[i].unit + "<br>");
                                    }*/
                                }else{
                                    
                                }
                            }
                        });
    				}
    	    	},
    	    	error: function(err) {
    	    		$("#oseba").html("<span>Prišlo je do napake '" +JSON.parse(err.responseText).userMessage + "'!");
    	    	}
    		});
    	}
    }
    
    /* PREBIRANJE PODATKOV IZ OPEANWEATHER IN MASTERDETAIL */
    naSpremembo=function(callback){
        var izbira = document.getElementById("izbor");
        var mesto = izbira.options[izbira.selectedIndex].text;
        callback(mesto);
    }
    
    initMap=function(mesto) {
        var lokacija;
        switch(mesto){
            case "Ljubljana": lokacija={lat: 46.053624, lng: 14.521354}; break; //46.053624, 14.521354
            case "Maribor": lokacija={lat: 46.553117, lng: 15.648563}; break; //46.553117, 15.648563
            case "Koper": lokacija={lat: 45.544416, lng: 13.687089}; break; // 45.544416, 13.687089
            case "Nova Gorica": lokacija={lat: 45.931737, lng: 13.641105}; break; //45.931737, 13.641105
                    
        }
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: lokacija
        });
        var marker = new google.maps.Marker({
          position: lokacija,
          map: map
        });
      }
    
}); // konec document ready
